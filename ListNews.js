import React from 'react';
import { 
  StyleSheet,
  View, 
  Dimensions, FlatList, Text} from 'react-native';
import NewsRow from './NewsRow';

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height


export default class ListNews extends React.Component {

  constructor(props){
    super(props);
    this.source_url = '';
    this.type = props.route.params.type;
    switch(props.route.params.type) {
      case 'mostpopular' : 
        this.source_url = 'https://api.nytimes.com/svc/mostpopular/v2/viewed/1.json?api-key=AwXnKzklTLTpcierTSpWokopOkSyrwtZ';
        break;
      case 'business' :
        this.source_url = 'https://api.nytimes.com/svc/topstories/v2/business.json?api-key=AwXnKzklTLTpcierTSpWokopOkSyrwtZ';
        break;
      case 'home' :
        this.source_url = 'https://api.nytimes.com/svc/topstories/v2/home.json?api-key=AwXnKzklTLTpcierTSpWokopOkSyrwtZ';  
        break;
    }
    console.log(this.source_url);
    this.navigation = props.navigation;
    this.state = {
      news: []
    }
  }

  componentDidMount(){
    console.log('componentDidMount');
    fetch(this.source_url)
    .then(response => response.json())
    .then((responseJson)=> {
      console.log('We are here');
      this.setState({
        news: responseJson.results
      })
    })
    .catch(error => console.log(error)) //to catch the errors if any
  }

  render() {
    return (
      <View style={styles.container}>
        <FlatList
          style={styles.list}
          data={this.state.news}
          renderItem={
            ({item}) => <NewsRow
            item={item}
            type_article={this.type}
            navigation={this.navigation}
          />
          }
          >
        </FlatList>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    flexDirection: 'row'
  },

  list: {
    
  }
});