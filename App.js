import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { 
  StyleSheet} from 'react-native';

const Stack = createStackNavigator();
import DetailNews from './DetailNews';
import Tabbar from './Tabbar';

export default class App extends React.Component {
  render() {
    return (
      <NavigationContainer>
        <Stack.Navigator initialRouteName="List news"
          screenOptions={{
            headerStyle: {
              backgroundColor: '#00ACEE',
            },
            headerTintColor: '#ffff',
          }}>
          <Stack.Screen name="List news" component={Tabbar} />
          <Stack.Screen name="Detail" component={DetailNews} />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
