import React from 'react';
import { 
  StyleSheet, Text, 
  View, Image, TextInput, 
  Dimensions, TouchableOpacity } from 'react-native';

import WebView from 'react-native-webview';

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height

export default class DetailNews extends React.Component {

  constructor(props) {
    super(props);
    this.article_url = props.route.params.article_url;
  }

  render() {
    console.log(this.article_url);
    return (
      <WebView source={{ uri: this.article_url}} />
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
  },
});
