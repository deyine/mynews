import React from 'react';

import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import ListNews from './ListNews';

const Tab = createMaterialTopTabNavigator();

export default class Tabbar extends React.Component {
  render() {
    return (
      <Tab.Navigator>
        <Tab.Screen name="Most Popular" component={ListNews} initialParams={{type: 'mostpopular'}} />
        <Tab.Screen name="Home" component={ListNews} initialParams={{type: 'home'}} />
        <Tab.Screen name="Business" component={ListNews} initialParams={{type: 'business'}} />
      </Tab.Navigator>
    )
  }
}