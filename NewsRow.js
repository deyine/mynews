
import React from 'react'
import { View, Text, Image, StyleSheet, TouchableWithoutFeedback } from 'react-native'

export default class NewsRow extends React.Component {

  constructor(props){
    super(props);
    this.navigation = props.navigation;
    console.log(this.props.type_article);
    this.image_url = 'https://upload.wikimedia.org/wikipedia/commons/0/0a/No-image-available.png';
    if(this.props.type_article == 'mostpopular') {
      if(this.props.item.media.length > 0) {
        this.image_url = this.props.item.media[0]['media-metadata'][0].url;
      }
    }else {
      if(this.props.item.multimedia.length > 0) {
        this.image_url = this.props.item.multimedia[0].url
      }
    }
  }

  onItemClick = () => {
    console.log('onItemClick');
    this.navigation.navigate('Detail', {
      article_url: this.props.item.url
    })
  }

  render() {
    return (
      <TouchableWithoutFeedback onPress={this.onItemClick}>
        <View style={styles.row}>
          <Image style={styles.picture} source={{ uri: this.image_url }} />
          <View style={{width: 250}} >
            <Text style={styles.primaryText}>
              {this.props.item.title}
            </Text>
          </View>
        </View>
      </TouchableWithoutFeedback>
    )
  }
}

const styles = StyleSheet.create({
  row: { 
    flexDirection: 'row', 
    alignItems: 'center', 
    padding: 12 ,
    borderBottomWidth:1,
    borderBottomColor:'grey'  
  },
  picture: { width: 50, height: 50, borderRadius: 25, marginRight: 18 },
  primaryText: {
    fontWeight: 'bold',
    fontSize: 14,
    color: 'black',
    marginBottom: 4,
  },
  secondaryText: { color: 'grey' },

  icon: {
    borderBottomWidth:1,
    borderBottomColor:'grey'  ,
    flexDirection: 'row',
    alignItems: 'flex-end'
  }
});